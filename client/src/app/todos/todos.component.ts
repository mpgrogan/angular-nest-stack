import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.sass']
})
export class TodosComponent implements OnInit {
  todos: any;
  constructor(private http: HttpClient) {}

  getTodos() {
    this.http.get('/api/todos').subscribe(data => {
      console.log(data);
      this.todos = data;
    });
  }

  ngOnInit() {
    this.getTodos();
  }
}
