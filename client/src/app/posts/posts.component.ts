import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {
  posts: any;
  constructor(private http: HttpClient) {}

  getPosts() {
    this.http.get('/api/posts').subscribe(data => {
      console.log(data);
      this.posts = data;
    });
  }

  ngOnInit() {
    this.getPosts();
  }
}
