import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { join } from 'path';

@Injectable()
export class ServeHTMLMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    // check if the requested path is your api endpoint, if so then we have to return next()
    if (req.path.includes('api')) {
      return next();
    }

    res.sendFile(join(__dirname, '../dist/frontend/index.html'));
  }
}
