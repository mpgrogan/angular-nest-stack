import {
  Module,
  HttpModule,
  MiddlewareConsumer,
  RequestMethod
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServeHTMLMiddleware } from './app.middleware';

@Module({
  imports: [HttpModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ServeHTMLMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.GET });
  }
}
