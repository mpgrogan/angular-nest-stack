import { Controller, Get, Param, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { Response } from 'express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // get(@Res() res: Response) {
  //   res.sendFile('index.html', {
  //     root: '../dist/fullstack',
  //   });
  // }

  @Get('api/todos')
  getTodos() {
    return this.appService.getAllTodos();
  }

  @Get('api/todos/:id')
  getTodoById(@Param('id') id) {
    return this.appService.getTodo(id);
  }

  @Get('api/posts')
  getPosts() {
    return this.appService.getAllPosts();
  }
}
