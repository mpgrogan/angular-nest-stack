import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AxiosResponse } from 'axios';
import { Todo } from './interfaces/todo.interface';

@Injectable()
export class AppService {
  constructor(private readonly httpService: HttpService) {}

  // getHello(): string {
  //   return 'Hello World!';
  // }

  getAllTodos(): Observable<AxiosResponse<Todo[]>> {
    return this.httpService
      .get('http://jsonplaceholder.typicode.com/todos')
      .pipe(map(response => response.data));
  }

  getTodo(id: number): Observable<AxiosResponse<Todo>> {
    return this.httpService
      .get('http://jsonplaceholder.typicode.com/todos/' + id)
      .pipe(map(response => response.data));
  }

  getAllPosts(): Observable<AxiosResponse<any[]>> {
    return this.httpService
      .get('http://jsonplaceholder.typicode.com/posts')
      .pipe(map(response => response.data));
  }
}
