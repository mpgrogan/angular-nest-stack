# README #

## Angular + NestJs ##

### Basic Setup of Angular 8 app with production ready webserver ###

### Installation ###
Follow readme to install nvm on your machine

[https://github.com/nvm-sh/nvm/blob/master/README.md](https://github.com/nvm-sh/nvm/blob/master/README.md)

Once installed, run

	nvm install --lts

Check which version of node is running

	nvm list
	
If running a different version than lts (v10.16.0 at time of writing), run

	nvm use --lts
	
Now, install the latest angular

	npm install -g @angular/cli
	
And install nest

	npm install -g @nestjs/cli
	
In the cloned directory, run **npm install** in both client and server folders

**And you should be all set!**

### For Development ###
Run both the default webpack server and the node server in separate terminals in project directory

	cd client

	ng serve --proxy-config proxy.config.json

This will start the webpack server on port 4200 with config to use our server. To view, put this address in your browser

	http://localhost:4200

**In another terminal**

	cd server

	npm run start:dev

Now you can develop with a watch on changes to both frontend and backend with autoreloading

### For Production ###

Inside client directory run

	ng build

Then inside server directory run

	npm start:prod


